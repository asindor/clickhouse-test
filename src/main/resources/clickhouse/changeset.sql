CREATE TABLE IF NOT EXISTS history
(
    visit_at   DATETIME DEFAULT now(),
    visit_type String,
    page_url   String,
    client_id  UUID,
    user_id    UUID
) ENGINE = MergeTree ORDER BY (visit_at);

-- Buffer table for migration from pg
CREATE TABLE IF NOT EXISTS history_buf
(
    visit_at   DATETIME('UTC') DEFAULT now(),
    visit_type String,
    page_url   String,
    client_id  UUID,
    user_id    UUID,
    row_id     UInt64
-- original instructions says use memory engine and it's supposed to pass data to main table
-- but it's not happening. Migration tool works weird.
) ENGINE = MergeTree ORDER BY (visit_at);

-- I don't understand how it's supposed to working
-- CREATE TABLE IF NOT EXISTS history_pg
-- (
--     visit_at   DATETIME('UTC') DEFAULT now(),
--     visit_type String,
--     page_url   String,
--     client_id  UUID     ,
--     user_id    UUID
-- ) ENGINE PostgreSQL('postgres:5432', 'postgres', 'history', 'postgres', 'pass', 'public');


-- Buffer table for migration from pg
CREATE TABLE IF NOT EXISTS history_buf_2
(
    visit_at   DATETIME('UTC') DEFAULT now(),
    visit_type String,
    page_url   String,
    client_id  UUID,
    user_id    UUID,
    row_id     UInt64
) ENGINE = MergeTree()
      ORDER BY visit_at
      PARTITION BY xxHash64(toString(client_id))
      PRIMARY KEY (client_id, visit_at)
      SETTINGS index_granularity = 8192;;