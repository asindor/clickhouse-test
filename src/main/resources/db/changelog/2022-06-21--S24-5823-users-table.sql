--liquibase formatted sql

--changeset orlov:S24-5823
CREATE TABLE history
(
    visit_at   TIMESTAMP NOT NULL DEFAULT (now() at time zone 'utc'),
    visit_type VARCHAR(50),
    page_url   VARCHAR(255),
    client_id  UUID      NOT NULL,
    user_id    UUID      NOT NULL
);

CREATE INDEX idx_history_client_id ON history(client_id);
CREATE INDEX idx_history_user_id ON history(user_id);
CREATE INDEX idx_history_visit_at ON history(visit_at);