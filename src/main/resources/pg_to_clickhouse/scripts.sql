ALTER TABLE history REPLICA IDENTITY FULL;
CREATE PUBLICATION pg2ch_pub FOR TABLE history;
SELECT * FROM pg_create_logical_replication_slot('pg2ch_slot', 'pgoutput');