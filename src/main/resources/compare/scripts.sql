
-- Count all records - total 225M records
-- Clickhouse
select count(*) from history_buf; -- 11ms
-- Postgres
select count(*) from history; -- 225M 1m42s
-- Get records for one user
-- Clickhouse
select *
from history_buf
where user_id = '313bb882-7e67-4681-bfba-7b8c0959fb20'; -- cold 3s600ms hot 800ms
-- Postgres
select *
from history
where user_id = '97122eb8-5a27-4829-a0aa-dd6a895f8d6a'; --66ms cold, 0.100ms hot

-- Table sizes
-- Clickhouse
select concat(database, '.', table)                         as table,
       formatReadableSize(sum(bytes))                       as size,
       sum(rows)                                            as rows,
       max(modification_time)                               as latest_modification,
       sum(bytes)                                           as bytes_size,
    any(engine)                                          as engine,
       formatReadableSize(sum(primary_key_bytes_in_memory)) as primary_keys_size
from system.parts
where active
group by database, table;
-- 16.5 GB                                                                            order by bytes_size desc;
-- Postgres
SELECT pg_size_pretty(pg_table_size('history')); -- 34 GB (+23 GB for indexes, total 58Gb)

-- Get count of unique users
-- Clickhouse
select count(distinct user_id) from history_buf; -- 10s
-- Postgres
select count(distinct user_id)
from history; -- 7m16s
-- Get actions count for last 1 month
-- Clickhouse
select count(*)
from history_buf where visit_at > now() - INTERVAL 1 MONTH; -- 66ms
-- Postgres
select count(*)
from history where visit_at > now()::timestamp - '1 month'::interval; -- 1m33s
--