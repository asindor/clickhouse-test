package ru.sport24.template.config

import com.zaxxer.hikari.HikariConfig
import com.zaxxer.hikari.HikariDataSource
import org.jooq.DSLContext
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.autoconfigure.liquibase.LiquibaseDataSource
import org.springframework.boot.convert.DurationStyle
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Primary
import org.springframework.transaction.PlatformTransactionManager
import ru.sport24.postgres.DatabaseConfigurationFactory
import ru.sport24.postgres.buildNodes
import java.time.Duration
import javax.sql.DataSource

@Configuration
class Sport24DatasourceConfiguration(
    @Autowired private val databaseConfigurationFactory: DatabaseConfigurationFactory,
) {

    @LiquibaseDataSource
    @Primary
    @Bean
    fun dataSource(): DataSource {
        return databaseConfigurationFactory.dataSource(false, "templateDatasource")
    }

    @Primary
    @Bean
    fun transactionManager(dataSource: DataSource): PlatformTransactionManager {
        return databaseConfigurationFactory.platformTransactionManager(dataSource)
    }

    @Bean
    fun dslContext(
        dataSource: DataSource,
        platformTransactionManager: PlatformTransactionManager,
    ): DSLContext {
        return databaseConfigurationFactory.jooqContextDsl(dataSource, platformTransactionManager)
    }

//    @Bean
//    fun clickHouseDatasource(): HikariDataSource {
//        val hikariConfig = HikariConfig()
//        val maximumPoolSize = 10
//        val connectionTimeoutDuration = Duration.ofSeconds(30)
//        hikariConfig.connectionTimeout = connectionTimeoutDuration.toMillis()
//        hikariConfig.leakDetectionThreshold = 0
//        hikariConfig.jdbcUrl = "jdbc:clickhouse://localhost:8123/default"
//        hikariConfig.username = user
//        hikariConfig.driverClassName = "org.postgresql.Driver"
//        hikariConfig.password = password
//        hikariConfig.isAutoCommit = false
//        hikariConfig.maximumPoolSize = maximumPoolSize
//        hikariConfig.poolName = datasourceName
//        return HikariDataSource(hikariConfig)
//    }
}
