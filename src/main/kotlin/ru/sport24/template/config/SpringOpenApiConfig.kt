package ru.sport24.template.config

import io.swagger.v3.oas.annotations.OpenAPIDefinition
import io.swagger.v3.oas.annotations.info.Info
import io.swagger.v3.oas.models.security.SecurityRequirement
import io.swagger.v3.oas.models.security.SecurityScheme
import org.springdoc.core.GroupedOpenApi
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import ru.sport24.template.controller.PRIVATE_PATH
import ru.sport24.template.controller.PUBLIC_PATH
import ru.sport24.template.controller.SERVICE_NAME

const val API_GATEWAY_SECURITY_SCHEME = "api-gateway"

@OpenAPIDefinition(
    info = Info(
        title = "Sport24 $SERVICE_NAME API",
        version = "1.0.0"
    )
)
@Configuration
class SpringOpenApiConfig {

    @Bean
    fun publicApi(): GroupedOpenApi {
        return GroupedOpenApi.builder()
            .group("public")
            .pathsToMatch("$PUBLIC_PATH/**")
            .build()
    }

    @Bean
    fun adminApi(): GroupedOpenApi {
        return GroupedOpenApi.builder()
            .group("admin")
            .addOpenApiCustomiser { openAPI ->
                openAPI.components.addSecuritySchemes(
                    API_GATEWAY_SECURITY_SCHEME,
                    SecurityScheme().type(SecurityScheme.Type.HTTP)
                        .scheme("bearer").bearerFormat("JWT")
                        .`in`(SecurityScheme.In.HEADER).name("Authorization")
                )
                openAPI.addSecurityItem(SecurityRequirement().addList(API_GATEWAY_SECURITY_SCHEME))
            }
            .pathsToMatch("$PRIVATE_PATH/**")
            .build()
    }
}
