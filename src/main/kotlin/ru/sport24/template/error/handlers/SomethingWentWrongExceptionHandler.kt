package ru.sport24.template.error.handlers

import me.alidg.errors.Argument.arg
import me.alidg.errors.HandledException
import me.alidg.errors.WebErrorHandler
import org.springframework.http.HttpStatus
import org.springframework.stereotype.Component
import ru.sport24.template.error.SomethingWentWrongException

@Component
class SomethingWentWrongExceptionHandler : WebErrorHandler {
    override fun canHandle(exception: Throwable?): Boolean = exception is SomethingWentWrongException

    override fun handle(exception: Throwable?): HandledException {
        exception as SomethingWentWrongException
        val arguments = mapOf(
            "something_wrong" to listOf(
                arg("id", exception.someId),
                arg("cause", "Value is 42 put another value")
            )
        )
        return HandledException("something_wrong", HttpStatus.BAD_REQUEST, arguments)
    }
}
