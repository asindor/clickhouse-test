package ru.sport24.template.controller.public

import io.swagger.v3.oas.annotations.Operation
import io.swagger.v3.oas.annotations.tags.Tag
import mu.KotlinLogging
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.validation.annotation.Validated
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import ru.sport24.template.controller.PUBLIC_PATH_V1
import ru.sport24.template.repository.HistoryRepository
import java.time.LocalDateTime
import java.util.*

val READ_MATERIAL = "READ_MATERIAL"
val READ_MATERIAL_10 = "READ_MATERIAL_10_SEC"
val COMMENT_MATERIAL = "COMMENT_MATERIAL"

@Validated
@Tag(name = "something", description = "API doing something")
@RestController
@RequestMapping(PUBLIC_PATH_V1)
class TemplatePublicController(
    @Autowired private val historyRepository: HistoryRepository,
) {

    private val logger = KotlinLogging.logger {}

    @Operation(
        summary = "Get something",
        description = "Get something when id is not 42",
    )
    @GetMapping
    fun saveSyntheticDataToDatabase() {
        // 20_000_000 users each would have 40 history visits every week
        LongRange(0, 200).map {
            val all = LongRange(0, 100_000).map { generateUserHistory() }
                .flatten()
                .shuffled()
            all.parallelStream().forEach { historyRepository.saveRecord(it) }
            logger.info("Saved 100_000 users history")
        }
    }

    fun generateUserHistory(): List<History> {
        val clientId = UUID.randomUUID()
        val userId = UUID.randomUUID()
        val strings = listOf(READ_MATERIAL, READ_MATERIAL_10, COMMENT_MATERIAL)
        return LongRange(0, kotlin.random.Random.nextLong(41, 60))
            .map {
                val randomMaterial = UUID.randomUUID()
                History(
                    visitAt = LocalDateTime.now().minusDays(it),
                    visitType = strings.random(),
                    pageUrl = "https://sport24.ru/football/news/$randomMaterial",
                    clientId = clientId,
                    userId = userId,
                )
            }

    }
}

data class History(
    val visitAt: LocalDateTime,
    val visitType: String,
    val pageUrl: String,
    val clientId: UUID,
    val userId: UUID,
)
