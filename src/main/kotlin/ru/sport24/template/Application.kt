package ru.sport24.template

import ch.sbb.esta.openshift.gracefullshutdown.GracefulshutdownSpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.autoconfigure.web.servlet.error.ErrorMvcAutoConfiguration
import org.springframework.boot.context.properties.ConfigurationPropertiesScan

@SpringBootApplication(
    excludeName = [
        "org.springframework.boot.autoconfigure.web.servlet.error.ErrorMvcAutoConfiguration",
        "org.springframework.boot.actuate.autoconfigure.metrics.LogbackMetricsAutoConfiguration",
        "org.springframework.boot.actuate.autoconfigure.metrics.SystemMetricsAutoConfiguration",
        "org.springframework.boot.autoconfigure.gson.GsonAutoConfiguration",
        "org.springframework.boot.autoconfigure.jooq.JooqAutoConfiguration",
    ],
    scanBasePackages = ["ru.sport24"]
)
@ConfigurationPropertiesScan
class Application

fun main() {
    GracefulshutdownSpringApplication.run(Application::class.java)
}
