package ru.sport24.template.repository

import org.jooq.DSLContext
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Repository
import org.springframework.transaction.annotation.Transactional
import ru.sport24.synthetic.database.Tables
import ru.sport24.synthetic.database.tables.records.HistoryRecord
import ru.sport24.template.controller.public.History

@Repository
class HistoryRepository(
    @Autowired private val dsl: DSLContext,
) {

    /**
     * SELECT pg_size_pretty( pg_table_size('history') );
     */
    @Transactional
    fun saveRecord(history: History) {
        dsl.insertInto(Tables.HISTORY).set(
            HistoryRecord(
                history.visitAt,
                history.visitType,
                history.pageUrl,
                history.clientId,
                history.userId
            )
        ).execute()
    }
}

